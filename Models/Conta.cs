﻿using System.ComponentModel.DataAnnotations;

namespace ProjetoLogin.Models
{
    public class Conta
    {
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme a senha")]
        [Compare("Password", ErrorMessage = "As senhas não conferem")]
        public string ConfirmarPassword { get; set; }
    }
}
