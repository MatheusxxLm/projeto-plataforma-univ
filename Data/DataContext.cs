﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjetoLogin.Models;

namespace ProjetoLogin.Data
{
    public class DataContext : IdentityDbContext
    {
        public DataContext(DbContextOptions<DataContext> opts): base(opts)
        {
            
        }
        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Faculdade> Faculdades { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin<string>>()
            .HasNoKey();
            modelBuilder.Entity<IdentityUserRole<string>>()
            .HasNoKey();
            modelBuilder.Entity<IdentityUserToken<string>>()
            .HasNoKey();

            modelBuilder.Entity<Aluno>()
                .HasKey(a => new { a.Id, a.CPF });
        }
    }
}
