﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetoLogin.Models
{
    public class Aluno
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Key]
        public string CPF { get; set; }
        [Required]
        public string Nome { get; set; }
        public List<Curso> Cursos { get; set; }
        public List<Faculdade> Faculdade { get; set; }

        public List<Curso> ObterCursos()
        {
            if (Cursos != null)
            {
                return Cursos;
            }
            else
            {
                return new List<Curso>();
            }
        }

    }
}
