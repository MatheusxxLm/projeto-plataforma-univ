﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjetoLogin.Migrations
{
    /// <inheritdoc />
    public partial class second : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FaculdadeId",
                table: "Alunos");

            migrationBuilder.CreateTable(
                name: "CursoFaculdade",
                columns: table => new
                {
                    CursosId = table.Column<int>(type: "int", nullable: false),
                    FaculdadesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CursoFaculdade", x => new { x.CursosId, x.FaculdadesId });
                    table.ForeignKey(
                        name: "FK_CursoFaculdade_Cursos_CursosId",
                        column: x => x.CursosId,
                        principalTable: "Cursos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CursoFaculdade_Faculdades_FaculdadesId",
                        column: x => x.FaculdadesId,
                        principalTable: "Faculdades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CursoFaculdade_FaculdadesId",
                table: "CursoFaculdade",
                column: "FaculdadesId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CursoFaculdade");

            migrationBuilder.AddColumn<int>(
                name: "FaculdadeId",
                table: "Alunos",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
