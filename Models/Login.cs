﻿using System.ComponentModel.DataAnnotations;

namespace ProjetoLogin.Models
{
    public class Login
    {
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Lembrar-me")]
        public bool Lembre { get; set; }
    }
}
