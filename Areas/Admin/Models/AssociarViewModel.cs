﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace ProjetoLogin.Areas.Admin.Models
{
    public class AssociarViewModel
    {
        [Display(Name = "Selecione um Usuário")]
        public string SelectedUserId { get; set; }

        [Display(Name = "Selecione uma Função")]
        public string SelectedRole { get; set; }

        public List<SelectListItem>? Users { get; set; }
        public List<SelectListItem>? Roles { get; set; }
    }
}
