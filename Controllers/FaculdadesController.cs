﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoLogin.Data;
using ProjetoLogin.Models;

namespace ProjetoLogin.Controllers
{
    public class FaculdadesController : Controller
    {
        private readonly DataContext _context;

        public FaculdadesController(DataContext context)
        {
            _context = context;
        }

        // GET: Alunos
        public async Task<IActionResult> Index()
        {
            var Faculdades = await _context.Faculdades.AsNoTracking().ToListAsync();    
            return View(Faculdades);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Create(Faculdade faculdade)
        {
            if (ModelState.IsValid)
            {
                _context.Add(faculdade);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            // Se o ModelState não for válido, retorne a view com o aluno para exibir os erros de validação
            return View(faculdade);
        }
    }
}
