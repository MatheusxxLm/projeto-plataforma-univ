﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjetoLogin.Migrations
{
    /// <inheritdoc />
    public partial class aluno : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlunoCurso_Alunos_AlunosId",
                table: "AlunoCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AlunoFaculdade_Alunos_AlunosId",
                table: "AlunoFaculdade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Alunos",
                table: "Alunos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlunoFaculdade",
                table: "AlunoFaculdade");

            migrationBuilder.DropIndex(
                name: "IX_AlunoFaculdade_FaculdadeId",
                table: "AlunoFaculdade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlunoCurso",
                table: "AlunoCurso");

            migrationBuilder.DropIndex(
                name: "IX_AlunoCurso_CursosId",
                table: "AlunoCurso");

            migrationBuilder.AddColumn<string>(
                name: "CPF",
                table: "Alunos",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AlunosCPF",
                table: "AlunoFaculdade",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AlunosCPF",
                table: "AlunoCurso",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Alunos",
                table: "Alunos",
                columns: new[] { "Id", "CPF" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlunoFaculdade",
                table: "AlunoFaculdade",
                columns: new[] { "FaculdadeId", "AlunosId", "AlunosCPF" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlunoCurso",
                table: "AlunoCurso",
                columns: new[] { "CursosId", "AlunosId", "AlunosCPF" });

            migrationBuilder.CreateIndex(
                name: "IX_AlunoFaculdade_AlunosId_AlunosCPF",
                table: "AlunoFaculdade",
                columns: new[] { "AlunosId", "AlunosCPF" });

            migrationBuilder.CreateIndex(
                name: "IX_AlunoCurso_AlunosId_AlunosCPF",
                table: "AlunoCurso",
                columns: new[] { "AlunosId", "AlunosCPF" });

            migrationBuilder.AddForeignKey(
                name: "FK_AlunoCurso_Alunos_AlunosId_AlunosCPF",
                table: "AlunoCurso",
                columns: new[] { "AlunosId", "AlunosCPF" },
                principalTable: "Alunos",
                principalColumns: new[] { "Id", "CPF" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlunoFaculdade_Alunos_AlunosId_AlunosCPF",
                table: "AlunoFaculdade",
                columns: new[] { "AlunosId", "AlunosCPF" },
                principalTable: "Alunos",
                principalColumns: new[] { "Id", "CPF" },
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlunoCurso_Alunos_AlunosId_AlunosCPF",
                table: "AlunoCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AlunoFaculdade_Alunos_AlunosId_AlunosCPF",
                table: "AlunoFaculdade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Alunos",
                table: "Alunos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlunoFaculdade",
                table: "AlunoFaculdade");

            migrationBuilder.DropIndex(
                name: "IX_AlunoFaculdade_AlunosId_AlunosCPF",
                table: "AlunoFaculdade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlunoCurso",
                table: "AlunoCurso");

            migrationBuilder.DropIndex(
                name: "IX_AlunoCurso_AlunosId_AlunosCPF",
                table: "AlunoCurso");

            migrationBuilder.DropColumn(
                name: "CPF",
                table: "Alunos");

            migrationBuilder.DropColumn(
                name: "AlunosCPF",
                table: "AlunoFaculdade");

            migrationBuilder.DropColumn(
                name: "AlunosCPF",
                table: "AlunoCurso");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Alunos",
                table: "Alunos",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlunoFaculdade",
                table: "AlunoFaculdade",
                columns: new[] { "AlunosId", "FaculdadeId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlunoCurso",
                table: "AlunoCurso",
                columns: new[] { "AlunosId", "CursosId" });

            migrationBuilder.CreateIndex(
                name: "IX_AlunoFaculdade_FaculdadeId",
                table: "AlunoFaculdade",
                column: "FaculdadeId");

            migrationBuilder.CreateIndex(
                name: "IX_AlunoCurso_CursosId",
                table: "AlunoCurso",
                column: "CursosId");

            migrationBuilder.AddForeignKey(
                name: "FK_AlunoCurso_Alunos_AlunosId",
                table: "AlunoCurso",
                column: "AlunosId",
                principalTable: "Alunos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlunoFaculdade_Alunos_AlunosId",
                table: "AlunoFaculdade",
                column: "AlunosId",
                principalTable: "Alunos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
