﻿using Microsoft.EntityFrameworkCore;

namespace ProjetoLogin.Models
{
    public class Curso
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public List<Aluno>? Alunos { get; set; }
        public List<Faculdade>? Faculdades { get; set; }

        public static List<Curso> ObterCursos(DbContext context)
        {
            List<Curso> cursos = context.Set<Curso>().ToList();
            return cursos;
        }
    }
}
