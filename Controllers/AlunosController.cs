﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjetoLogin.Data;
using ProjetoLogin.Models;

namespace ProjetoLogin.Controllers
{

    public class AlunosController : Controller
    {
        private readonly DataContext _context;

        public AlunosController(DataContext context)
        {
            _context = context;
        }

        // GET: Alunos
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var alunos = await _context.Alunos.AsNoTracking().Include(x => x.Faculdade).Include(x => x.Cursos).ToListAsync();
            return View(alunos);
        }

        // GET: Alunos/Details/5
        [Authorize(Roles = "Admin, Gerente, User")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Alunos == null)
            {
                return NotFound();
            }

            var aluno = await _context.Alunos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aluno == null)
            {
                return NotFound();
            }

            return View(aluno);
        }

        // GET: Alunos/Create
        [Authorize(Roles = "Admin, Gerente")]
        public IActionResult Create()
        {
            // AQUI ELE NAO QUER UM REGISTRO, E SIM UM OBJETO
            var cursos = _context.Cursos.ToList();
            var faculdade = _context.Faculdades.ToList();
            var aluno = new Aluno
            {
                Cursos = cursos,
                Faculdade = faculdade
            };
            return View(aluno);
        }

        // POST: Alunos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Aluno aluno, int cursos, int faculdade, string cpf)
        {
            if (ModelState.IsValid)
            {
                bool cursoExisteNaFaculdade = await _context.Cursos
                .AnyAsync(c => c.Id == cursos && c.Faculdades.Any(f => f.Id == faculdade));

                // Precisa ser NULL para ser válido
                var CpfExiste = await _context.Alunos.FirstOrDefaultAsync(x => x.CPF == cpf);
                if (CpfExiste != null)
                {
                    return NotFound("Aluno já existente");
                }

                if (!cursoExisteNaFaculdade)
                {
                    // Se o curso não existir na faculdade, retorne um erro ou mensagem apropriada
                    return NotFound("O Curso não pertence à Faculdade selecionada.");
                }

                aluno.Cursos = await _context.Cursos.Where(x => x.Id == cursos).ToListAsync();
                aluno.Faculdade = await _context.Faculdades.Where(x => x.Id == faculdade).ToListAsync();
                _context.Add(aluno);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            // Se o ModelState não for válido, retorne a view com o aluno para exibir os erros de validação
            return View(aluno);
        }

        // GET: Alunos/Edit/5
        [Authorize(Roles = "Admin, Gerente")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Alunos == null)
            {
                return NotFound();
            }

            var aluno = await _context.Alunos.FindAsync(id);
            if (aluno == null)
            {
                return NotFound();
            }
            return View(aluno);
        }

        // POST: Alunos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nome")] Aluno aluno)
        {
            if (id != aluno.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aluno);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlunoExists(aluno.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(aluno);
        }

        // GET: Alunos/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Alunos == null)
            {
                return NotFound();
            }

            var aluno = await _context.Alunos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (aluno == null)
            {
                return NotFound();
            }

            return View(aluno);
        }

        // POST: Alunos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Alunos == null)
            {
                return Problem("Entity set 'DataContext.Alunos'  is null.");
            }
            var aluno = await _context.Alunos.FindAsync(id);
            if (aluno != null)
            {
                _context.Alunos.Remove(aluno);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AlunoExists(int id)
        {
          return (_context.Alunos?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        [HttpGet]
        public async Task<IActionResult> GetAlunos()
        {
            var alunos = await _context.Alunos.ToListAsync();
            return Json(alunos); // Retorna a lista de alunos como JSON
        }
    }
}
