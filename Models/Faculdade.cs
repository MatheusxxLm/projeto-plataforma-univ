﻿using ProjetoLogin.Controllers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetoLogin.Models
{
    public class Faculdade
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Nome { get; set; }
        public List<Aluno>? Alunos { get; set; }
        public List<Curso>? Cursos { get; set; }
    }
}
