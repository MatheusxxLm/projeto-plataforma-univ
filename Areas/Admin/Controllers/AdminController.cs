﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjetoLogin.Areas.Admin.Models;
using ProjetoLogin.Data;
using System.Data;

namespace ProjetoLogin.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly DataContext _context;

        public AdminController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, DataContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;

        }
        [HttpGet]
        public IActionResult Index()
        {
            var users = _userManager.Users.ToList(); // Obtenha a lista de usuários

            return View(users);
        }
        // este metodo chama a view Index (Associar) e popula os campos com os dados existentes
        [HttpGet]
        public async Task<IActionResult> Associar()
        {
            var users = await _userManager.Users.ToListAsync();
            var roles = await _roleManager.Roles.ToListAsync();

            var viewModel = new AssociarViewModel
            {
                Users = users.Select(u => new SelectListItem
                {
                    Value = u.Id,
                    Text = u.Email
                }).ToList(),
                Roles = roles.Select(r => new SelectListItem
                {
                    Value = r.Id,
                    Text = r.Name
                }).ToList()
            };
            // retorna a View associar, com os campos populados
            return View(viewModel);
        }


        [HttpPost]
        public IActionResult Associar(AssociarViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var userId = viewModel.SelectedUserId;
                var roleId = viewModel.SelectedRole;

                try
                {
                    using (var connection = _context.Database.GetDbConnection())
                    {
                        connection.Open();

                        using (var command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.Text;
                            command.CommandText = $"INSERT INTO UserRoles (UserId, RoleId) VALUES ('{userId}', '{roleId}')";

                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                    }

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, $"Erro ao associar usuário à função: {ex.Message}");
                }
            }

            return View(viewModel);
        }
    }
}


//var user = _context.Users.FirstOrDefault(u => u.Id == viewModel.SelectedUserId);
//if (user == null)
//{
//    ModelState.AddModelError(string.Empty, "Usuário não encontrado.");
//    return View(viewModel);
//}

//var role = _context.Roles.FirstOrDefault(r => r.Id == viewModel.SelectedRole);
//if (role == null)
//{
//    ModelState.AddModelError(string.Empty, "Função não encontrada.");
//    return View(viewModel);
//}
